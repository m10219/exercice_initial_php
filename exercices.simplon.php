<?php

echo "Exercice 0";

echo "<br>";
// Déclarez une variable égale à 5. Affichez ce résultat.

$num="5";
echo $num;

echo "<br>";
echo "<br>";




echo "Exercice 1";
echo "<br>";
// Déclarez votre amour à Simplon à travers la phrase suivante "Simplon c'est la vie." en respectant la même syntaxe au caractère près. 
// Vous devez pour cela uniquement utiliser les variables suivantes :
$word = "c'est ";
$word2 = "vie.";
$word3 = "Simplon";
$word4 = "la";

echo $word3." ".$word." ".$word4." ".$word2;


echo "<br>";
echo "<br>";




echo "Exercice 2";
echo "<br>";
// Voici 2 variables, comment obtenir le nombre 3 en utilisant 1 seule fois ces 2 variables ?
$variable1=12;
$variable2=4;

echo $variable1 / $variable2;

echo "<br>";
echo "<br>";





echo "Exercice 3";
echo "<br>";

$var="clement";
$var1= "extrapolation";
// Comment puis-je obtenir 20 en utilisant 1 seule fois ces 2 variables ?
echo strlen($var) + strlen($var1);

echo "<br>";
echo "<br>";




echo "Exercice 4";
echo "<br>";

// Complétez ce code afin de faire apparaître la phrase “je maîtrise tellement le code maintenant” :

$maîtrise_du_code = 1000;

if ($maîtrise_du_code >= 1000) {
echo "“je maîtrise tellement le code maintenant”";
}



echo "<br>";
echo "<br>";





echo "Exercice 5";
echo "<br>";

$chiffre_fetiche_sandrine = 7;
$chiffre_fetiche_xavier = 130;
$chiffre_fetiche_andre = 8;
// A l'aide des variables à disposition et de la programmation PHP, intervertissez le chiffre de sandrine avec celui de Xavier. 
// Retirer ensuite le chiffre de Xavier concaténé avec celui de Andre à celui de Sandrine. 
// Affichez ensuite le résultat si celui-ci est inférieur à 50 uniquement.

$a=$chiffre_fetiche_sandrine;
$b=$chiffre_fetiche_xavier;
$c=$chiffre_fetiche_andre;

$chiffre_fetiche_sandrine=$b;
$chiffre_fetiche_xavier=$a;

$resultat=($chiffre_fetiche_sandrine-($c.$chiffre_fetiche_xavier));

if ($resultat < 50) {
    echo $resultat;
} else {
    echo "Resultat inférieur à 50";
}




echo "<br>";
echo "<br>";






echo "Exercice 6";
echo "<br>";

$compte = -100;
// Traduisez cette phrase en programmation PHP. Si votre compte en banque est supérieur à 0 alors j'afficherai un petit message qui dit "bravo, vous êtes un bon gestionnaire".
// Sinon j'afficherai un message qui dit "Vous faites vraiment n'importe quoi avec votre argent".

if ($compte>0) {
    echo "bravo, vous êtes un bon gestionnaire";
} else {
    echo "Vous faites vraiment n'importe quoi avec votre argent";
}



echo "<br>";
echo "<br>";



echo "Exercice 7";
echo "<br>";

$vent = rand(20, 35);
$houle = rand(18, 30);
$cadence_vague = rand(6, 10);

// Définissez un petit programme permettant de savoir si les conditions sont bonnes pour aller surfer.
// Vous afficherez un petit message en conséquence. 
// Les données des variables ne sont volontairement pas définies (?) afin qu'on puisse les modifier. Votre programme doit toujours fonctionner.

// Si le vent est à plus de 30 km/h, alors il faut s'assurer que la houle ne dépasse pas les 20. 
// Si c'est le cas et que la candence des vagues est au moins de 10, on peut aller surfer.

// Si le vent est à moins de 30 km/h, alors la houle ne doit juste pas dépasser les 30. 
// Si c'est le cas, la cadence des vagues peut aller jusqu'à 8. 

// Dans les autres cas, les conditions ne sont pas bonnes pour aller surfer, affichez-le !

if ($vent > 30) {
    if ($houle <= 20) {
        if ($cadence_vague >= 10) {
            echo "Les conditions sont bonnes pour aller surfer !";
    }
    else {
        echo "Reste chez toi !";
             }}
    else {
        echo "Reste chez toi !";
             }}   elseif ($vent <= 30) {
                if ($houle <= 30) {
                    if ($cadence_vague <= 8) {
                        echo "Les conditions sont bonnes pour aller surfer !";
    }
    else {
        echo "Reste chez toi !";
             }} else {
        echo "Reste chez toi !";
             }}
                    
// Actualiser pour afficher un résultat différent !


echo "<br>";
echo "<br>";
             



echo "Exercice 8";

$nombre_1 = 88;
$nombre_2 = 7;
$nombre_3 = 23;
$nombre_4 = 5;
$nombre_5 = 45;
$nombre_6 = 12;

// Réalisez un programme à l'aide de ces variables qui permets de :
// Déterminez quels sont les nombres multiples de 2 puis réalisez la soustraction entre ces nombres et ceux qui ne le sont pas.

// BONUS) aider les autres à trouver leurs erreurs sans JAMAIS donner la réponse !

// if (($nombre_1 && $nombre_2 && $nombre_3 && $nombre_4 && $nombre_5 && $nombre_6) %2 != 0) {
//     echo "le nombre est impair";
// }

$modulo1 = $nombre_1 % 2;
$modulo2 = $nombre_2 % 2;
$modulo3 = $nombre_3 % 2;
$modulo4 = $nombre_4 % 2;
$modulo5 = $nombre_5 % 2;
$modulo6 = $nombre_6 % 2;
 
$impair = 0;
$pair = 0;

echo "<br>";

if ($modulo1 != 0) {
    $impair = $nombre_1;
    echo "le nombre 1 est impair";
} else {
    $pair = $nombre_1;
    echo "le nombre 1 est pair";
}

echo "<br>";

if ($modulo2 != 0) {
    $impair = $impair+$nombre_2;
    echo "le nombre 2 est impair";
} else {
    $pair = $pair+$nombre_2;
    echo "le nombre 2 est pair";
}

echo "<br>";

if ($modulo3 != 0) {
    $impair = $impair+$nombre_3;
    echo "le nombre 3 est impair";
} else {
    $pair = $pair+$nombre_3;
    echo "le nombre 3 est pair";
}

echo "<br>";

if ($modulo4 != 0) {
    $impair = $impair+$nombre_4;
    echo "le nombre 4 est impair";
} else {
    $pair = $pair+$nombre_4;
    echo "le nombre 4 est pair";
}

echo "<br>";

if ($modulo5 != 0) {
    $impair = $impair+$nombre_5;
    echo "le nombre 5 est impair";
} else {
    $pair = $pair+$nombre_5;
    echo "le nombre 5 est pair";
}

echo "<br>";

if ($modulo6 != 0) {
    $impair = $impair+$nombre_6;
    echo "le nombre 6 est impair";
} else {
    $pair = $pair+$nombre_6;
    echo "le nombre 6 est pair";
}

echo "<br>";

echo "La somme des nombres pairs est ".$pair;
echo "<br>";
echo "La somme des nombres impairs est ".$impair;
echo "<br>";
echo "Il reste ".($pair-$impair);
